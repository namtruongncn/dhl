<?php
/**
 * Widget API: WP_Widget_New_Posts class
 *
 * @package WordPress
 * @subpackage Widgets
 * @since 4.4.0
 */

/**
 * Core class used to implement a Text widget.
 *
 * @since 2.8.0
 *
 * @see WP_Widget
 */
class WP_Widget_New_Posts extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_new_posts',
			'description' => __( 'Tìm kiếm mã vận đơn.' ),
			'customize_selective_refresh' => true,
		);
		$control_ops = array(
			'width' => 400,
			'height' => 350,
		);
		parent::__construct( 'widget_new_posts', __( 'PD: Bài viết mới nhất' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Text widget instance.
	 */
	public function widget( $args, $instance ) {

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$post_args = array(
			'posts_per_page'      => 2,
			'ignore_sticky_posts' => 1,
		);

		$post_query = new WP_Query( $post_args ); ?>

		<div class="list_posts row">

		<?php
		if ( $post_query->have_posts() ) :

			while ( $post_query->have_posts() ) : $post_query->the_post(); ?>

				<div class="col-md-6 col-xs-12">

					<a href="<?php the_permalink(); ?>" class="post_thumbnail"><?php the_post_thumbnail(); ?></a>

					<?php the_title( '<h3 class="post_title"><a href="' . get_the_permalink() . '">', '</a></h3>' ); ?>

					<div class="excerpt"><?php echo wp_trim_words( get_the_content(), 45 ); ?></div>

					<div class="read-more"><a href="<?php the_permalink(); ?>"><?php esc_html_e( 'Chi tiết', 'pd-theme' ); ?></a></div>

				</div>

			<?php endwhile;

			the_posts_pagination( array(
				'prev_text' => '<span class="screen-reader-text">' . esc_html__( 'Trang trước', 'pd-theme' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . esc_html__( 'Trang sau', 'pd-theme' ) . '</span>',
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Trang', 'pd-theme' ) . ' </span>',
			) );

			wp_reset_postdata();

		endif; ?>

		</div>

		<?php
		echo $args['after_widget'];
	}

	/**
	 * Handles updating settings for the current Text widget instance.
	 *
	 * @since 2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );

		return $instance;
	}

	/**
	 * Outputs the Text widget settings form.
	 *
	 * @since 2.8.0
	 * @since 4.8.0 Form only contains hidden inputs which are synced with JS template.
	 * @access public
	 * @see WP_Widget_Visual_Text::render_control_template_scripts()
	 *
	 * @param array $instance Current settings.
	 * @return void
	 */
	public function form( $instance ) {
		$instance = wp_parse_args(
			(array) $instance,
			array(
				'title' => '',
			)
		);
		$title = $instance['title'];
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
		<?php
	}
}
