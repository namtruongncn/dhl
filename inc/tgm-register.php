<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * @see https://github.com/TGMPA/TGM-Plugin-Activation/blob/develop/example.php
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 */

/**
 * Register the required plugins for this theme.
 */
function pd_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 */
	$plugins = array(
		array(
			'name'     => esc_html__( 'Meta Slider', 'pd-theme' ),
			'slug'     => 'ml-slider',
			'required' => true,
		),
	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 */
	$config = array(
		'id'           => 'pd-theme',
		'is_automatic' => true,
		'strings'      => array( 'nag_type' => 'notice-warning' ),
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'pd_register_required_plugins' );
