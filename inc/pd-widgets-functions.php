<?php
/**
 * Register Widgets
 *
 * @package PD_Theme
 */

require_once get_theme_file_path( '/inc/widgets/class-wp-widget-search-code.php' );
require_once get_theme_file_path( '/inc/widgets/class-wp-widget-support.php' );
require_once get_theme_file_path( '/inc/widgets/class-wp-widget-new-posts.php' );

function pd_register_widgets() {
	register_widget( 'WP_Widget_Search_Code' );
	register_widget( 'WP_Widget_Support' );
	register_widget( 'WP_Widget_New_Posts' );
}
add_action( 'widgets_init', 'pd_register_widgets' );
