<?php
/**
 * Copyright template part.
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 */

if ( ! pd_option( 'copyright', null, false ) ) {
	return;
}
?>

<div class="copyright text-right">
	<div class="container">
		<?php pd_option( 'copyright' ); ?>
	</div>
</div>
