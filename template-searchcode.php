<?php
/**
 * Template Name: Tra mã vận đơn
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?>

			<?php if ( isset( $_GET['code'] ) ) : ?>
				<iframe src="http://www.dhl.com.vn/vi/express/tracking.html?AWB=<?php echo strtoupper( $_GET['code'] ); ?>&brand=DHL" frameborder="0" width="100%" height="500px"></iframe>
			<?php else : ?>
				<?php echo 'No result found for your DHL query. Please try again.'; ?>
			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
